 (custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(inhibit-startup-screen t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; 起動時のサイズ,表示位置,フォントを指定
(setq initial-frame-alist
      (append (list
               '(width . 100)
               '(height . 45)
               '(top . 0)
               '(left . 0)
               )
              initial-frame-alist))
(setq default-frame-alist initial-frame-alist)

;; Remap font size
(global-set-key [(super ?+)] (lambda () (interactive) (text-scale-increase 1)))
(global-set-key [(super ?-)] (lambda () (interactive) (text-scale-increase -1)))
(global-set-key [(super ?0)] (lambda () (interactive) (text-scale-increase 0)))


(load "~/.emacs.d/whitespace")
(require 'whitespace)

;行番号
(global-linum-mode t)

;aspell 使ってない？
;(setq ispell-program-name "/opt/local/bin/aspell")

;;haskell-mode
(add-to-list 'load-path "~/.emacs.d/haskell-mode-2.8.0")
(require 'haskell-mode)
(require 'haskell-cabal)
(add-to-list 'auto-mode-alist '("\\.hs$" . haskell-mode))
(add-to-list 'auto-mode-alist '("\\.lhs$" . literate-haskell-mode))
(add-to-list 'auto-mode-alist '("\\.cabal\\'" . haskell-cabal-mode))
(add-to-list 'interpreter-mode-alist '("runghc" . haskell-mode))     ;#!/usr/bin/env runghc 用
(add-to-list 'interpreter-mode-alist '("runhaskell" . haskell-mode)) ;#!/usr/bin/env runhaskell 用

;; *.~ とかのバックアップファイルを作らない
(setq make-backup-files nil)
;; .#* とかのバックアップファイルを作らない
(setq auto-save-default nil)

;;ツールバー非表示
(tool-bar-mode 0)

;; ;ウィンドウの背景色
;; (set-foreground-color "white")
;; (set-background-color "#181818")

;; ;Modelineの色
;; (set-face-foreground 'mode-line "gray")
;; (set-face-background 'mode-line "#404040")

;; (setq default-frame-alist
;;       (append (list '(cursor-color . "white")) default-frame-alist))

;; ;fringeの色
;; (set-face-background 'fringe "#181818")
;; (fringe-mode (cons 1 0))

;scrollbar
(set-scroll-bar-mode nil)

;; 背景を半透明にする
(setq default-frame-alist
     (append (list
              '(alpha . (95 95))
              ) default-frame-alist))

;; 現在行をハイライト
(global-hl-line-mode t)
(custom-set-faces
'(hl-line ((t (:background "#333333"))))
)
;; ; 白地に黒字は 目が疲れるので、黒地に白字。
;; (set-foreground-color "white")
;; (set-background-color "#002833")

;; ;行番号の横にある余白
;; (set-face-background 'fringe "#224455")
;; (fringe-mode (cons 5 0) )

;; (setq font-lock-support-mode 'jit-lock-mode)
;; (if window-system (progn
;; (require 'font-lock)
;; (global-font-lock-mode t)
;; (add-hook 'font-lock-mode-hook '(lambda ()
;; (set-face-foreground 'font-lock-comment-face "orange3")
;; (set-face-foreground 'font-lock-string-face "salmon1")
;; (set-face-foreground 'font-lock-keyword-face "OrangeRed")
;; (set-face-foreground 'font-lock-function-name-face "aqua")
;; (set-face-bold-p 'font-lock-function-name-face "firebrick1")
;; (set-face-foreground 'font-lock-variable-name-face "GreenYellow")
;; (set-face-foreground 'font-lock-type-face "DeepSkyBlue1")
;; (set-face-foreground 'font-lock-builtin-face "purple")
;; (set-face-foreground 'font-lock-constant-face "green")
;; (set-face-foreground 'font-lock-warning-face "red")
;; (set-face-bold-p 'font-lock-warning-face nil)
;; ))))

;font
;Must be installed 'Ricty Diminished'
(set-face-attribute 'default nil :family "Ricty Diminished" :height 140)
(set-fontset-font "fontset-default" 'japanese-jisx0208 '("Ricty" . "iso10646-*"))

; Ctr-RET
; M-k   : kill
; Ctr-d : kill
; M-b   : fill by space
; M-n   : sequential number
(cua-mode t)
(setq cua-enable-cua-keys nil)

;;補完
(load "~/.emacs.d/auto-complete-1.3.1/popup")
(require 'popup)
(load "~/.emacs.d/auto-complete-1.3.1/auto-complete")
(require 'auto-complete)
(load "~/.emacs.d/auto-complete-1.3.1/auto-complete-config")
(require 'auto-complete-config)
(ac-config-default)
(add-to-list 'ac-modes 'r-mode)
;(add-to-list 'ac-modes 'org-mode)
;(add-to-list 'ac-modes 'text-mode)
(add-to-list 'ac-modes 'ruby-mode)
(global-auto-complete-mode t)

;; popup setting
(setq
      ;; ac-auto-show-menu 1
      ac-candidate-limit 20
      ;; ac-delay 1.0
      ;; ac-disable-faces (quote (font-lock-comment-face font-lock-doc-face))
      ;; ac-ignore-case 'smart
      ;; ac-menu-height 10
      ac-quick-help-delay 0.5
      ;; ac-quick-help-prefer-pos-tip t
      ;; ac-use-quick-help nil
)

;;ess
(load "~/.emacs.d/ess-13.05/lisp/ess-site")
(require 'ess-site)
;; # の数によってコメントのインデントの挙動が変わるのを無効にする
(setq ess-fancy-comments nil)
;; 起動時にワーキングディレクトリを尋ねられないようにする
(setq ess-ask-for-ess-directory nil)

;; R-iESSを起動していたらemacs終了時にプロセス残ってますけどと毎回聞かれるのがうざいので無視する
(add-hook 'comint-exec-hook
      (lambda () (process-kill-without-query (get-buffer-process (current-buffer)))))

;; org-mode
(add-to-list 'load-path "~/.emacs.d/org-mode/")
(add-to-list 'load-path "~/.emacs.d/org-mode/lisp/")

;; 折り返ししない for org-mode
(setq org-startup-truncated nil)

;; ditaa for org-mode
(setq org-ditaa-jar-path "/Applications/ditaa0_9/ditaa0_9.jar")
(defun bh/display-inline-images ()
(condition-case nil
(org-display-inline-images)
(error nil)))
(add-hook 'org-babel-after-execute-hook 'bh/display-inline-images 'append)

; Make babel results blocks lowercase
(setq org-babel-results-keyword "results")

(org-babel-do-load-languages
(quote org-babel-load-languages)
(quote ((emacs-lisp . t)
(dot . t)
(ditaa . t)
(R . t)
(python . t)
(ruby . t)
(gnuplot . t)
(clojure . t)
(sh . t)
(ledger . t)
(org . t)
(plantuml . t)
(latex . t))))

; Do not prompt to confirm evaluation
; This may be dangerous - make sure you understand the consequences
; of setting this -- see the docstring for details
(setq org-confirm-babel-evaluate nil)

; rhtml-mode
; edit for .erb
(add-to-list 'load-path "~/.emacs.d/rhtml")
(require 'rhtml-mode)

; org to tex
(setq org-latex-classes nil)
(add-to-list 'org-latex-classes
  '("jsarticle"
    "\\documentclass[a4j]{jsarticle}"
    ("\\section{%s}" . "\\section*{%s}")
    ("\\subsection{%s}" . "\\subsection*{%s}")
    ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
    ("\\paragraph{%s}" . "\\paragraph*{%s}")
    ("\\subparagraph{%s}" . "\\subparagraph*{%s}")
))
(setq org-latex-packages-alist
  '(("AUTO" "inputenc"  t)
    ("T1"   "fontenc"   t)
    ))

; markdown-mode
; http://jblevins.org/projects/markdown-mode/
; gfm-mode : GitHub Flavored Markdown mode
(add-to-list 'load-path "~/.emacs.d/markdown-mode")
(autoload 'markdown-mode "markdown-mode.el" "Major mode for editing Markdown files" t)
(setq auto-mode-alist (cons '("\\.md" . markdown-mode) auto-mode-alist))

; cmdをaltにする
;(setq ns-command-modifier (quote meta))

; org-mode TODO
;(setq org-agenda-files (list "~/Documents"))

; custom thema
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(load-theme 'spolsky t)

;現在行に下線を引かない
(global-hl-line-mode 0)

;mhc
(setq load-path
            (cons "~/nomlab/projects/mhc/emacs" load-path))
(autoload 'mhc "mhc" "Message Harmonized Calendar system." t)

;helm
;(add-to-list 'load-path "~/.emacs.d/helm")
;(require 'helm-config)
;(helm-mode 1)
; 自動補完を無効
;(custom-set-variables '(helm-ff-auto-update-initial-value nil))

;DTB
;; (add-to-list 'load-path "~/.emacs.d/elpa/emacs-request")
;; (add-to-list 'load-path "~/.emacs.d/elpa/emacs-file-reference-recorder")
;; (require 'file-reference-recorder)
;; (custom-set-variables '(frr:dtb-url "http://localhost:12001")'(frr:history-location "~/.file-reference-history"))

;ruby
(setq ruby-insert-encoding-magic-comment nil)
